# Naître enchantés

## Description
Il s'agit du GIT pour le site vitrine de l'entreprise "Naître, enchantés" (Ceci est un projet d'école. L'entreprise n'existe pas.).

## License
Ce projet est sous licence GNU GPLv3 - voir le fichier [LICENSE](LICENSE) pour plus de détails.